# Powervault-Kaluza Technical Integration: Guide to Publishing and Receiving Data

Powervault is introducing a new API for data collection and remote state control of its units over the MQTT protocol. This API follows the publish-subscribe model of bidirectional communication, and allows 3rd parties to access bespoke data streams in real time. Below is a guide to using Powervault's MQTT API. Kaluza can subscribe to topics to monitor data from individual units, groups of units or all units at once. Similarly, Powervault units can be set into specific charging and discharging states, whether individually or as a group, using secure encrypted channels on Amazon Web Services' IoT core service.

# Subscribing to Powervault data streams

## Topic names

Each Powervault unit publishes data to multiple MQTT topics on AWS IoT core. Kaluza have access to a range of bespoke topics which receive streaming data from participating units, comprising all the essential measurements about inverter status, batteries, charging state and so on. To access data for an individual unit, use this topic format:

```
p3/kaluza/{unitId}/measurements
```

Where `unitId` is a unique 8-character hexadecimal string (e.g. '005abcde'). The simplest way to track this data is to use the GUI on AWS IoT core. Subscribe to the topic above and the streaming MQTT messages are visible in real time as they are sent by the unit. Using MQTT's wildcard symbol, `#`, you can view individual data for all units participating in the Kaluza integration, using the topic selector: `p3/kaluza/#`

If a unit is also part of one or more 'group' of devices, for example, for aggregation, state control, analytics purposes, then it will send relevant data to the dedicated MQTT topic for each group. The group topic format is:
```
groups/kaluza/pv-test-group/measurements
```

Subscribers to a group will receive data from all participating units publishing data to that group.

## Data packet format

Streaming time series data from Powervault units follows a common format. An example message packet is as follows:

```json
{
  "UnitID": "",
  "ComponentSN": "",
  "DataType": "",
  "DeviceType": "",
  "StringID": "",
  "measurement": "",
  "time": "",
  "timestamp": "",
  "value": ""
}
```

Each message packet has a unit ID, a measurement, a timestamp, and a value, as well as other identifiers. Time series measurements packets from the unit use lowercase format for single word key names, and PascalCase format for multiple word key names and values. Depending on the volume of data being published each time, message packets can be individual data measurements, as above, or an array of several measurements:

```json
[
  {
    "UnitID": "",
    "ComponentSN": "",
    "DataType": "",
    "DeviceType": "",
    "StringID": "",
    "measurement": "",
    "time": "",
    "timestamp": "",
    "value": ""
  },
  {...},
  {...}
]
```


# Publishing instructions to Powervaults

## Topic names

You can send state control instructions to individual units or groups of units. To control a single unit, use the base topic:

```
p3/kaluza/{unitId}/...
```

Where `unitId` is a unique 8-character hexadecimal string (e.g. '005abcde'). The two distinct topic endpoints for state control are `schedule` and `stateOverride`:

```
p3/kaluza/{unitId}/schedule
p3/kaluza/{unitId}/stateOverride
```

The base topic for controlling groups of unit has a unique group name in it, rather than a unit ID:

```
groups/kaluza/kaluza-test-group/...
```

Use the same topic endpoints as with individual unit control:

```
groups/kaluza/kaluza-test-group/schedule
groups/kaluza/kaluza-test-group/stateOverride
```

`schedule` and `stateOverride` endpoints require valid JSON, and each type of endpoint requires its own schema format. If an invalid or incomplete JSON schema is sent to the unit, the instruction will be ignored. If a stateOverride message is sent to a schedule topic, it will also be ignored, and vice versa.

## Valid JSON: a quick refresher

- the top level object must be an associative array, or an array of such valid objects (associative array or array)
- use lower case throughout, with camelCase for multiword key names where applicable, e.g. "stateOverride"
- use double quotes for key names and string values, not single quotes
- no trailing comma after the final item inside an array or associative array

If the message packet breaks any of these rules, it will be ignored and the instruction will not be executed. Assuming the message is in a valid format, it is then validated to check that it conforms to the required schema for messages on that topic (`schedule` or `stateOverride`).

## Schedule messages

A valid schedule contains charging state instructions for a weekly period. When a valid schedule message is sent to a unit, it is saved to the unit's local storage, overwriting its existing schedule JSON file. This schedule is then followed by the unit until further notice; as the file is stored and accessed locally, the unit can follow a weekly charging schedule regardless of the quality of internet connection.

A schedule must be a valid JSON file with a key for each day of the week. Each key is an array of actions. An action must be an associative array/dictionary containing the following 3 keys: `state`, `start` and `end`. An optional 4th key, `setpoint` can be added to target a specific power output. A typical action, described in more detail below:

```json
{"state": "force-discharge", "start": 0, "end": 3600}
```

### state

A state is a **string** representing a distinct charging behaviour. The available charging states are:

```
normal
force-charge
force-discharge
only-charge
only-discharge
idle
```

### start

The start time is an **integer** representing the time of day in seconds elapsed since midnight. So a value of 3600 corresponds to 01:00, 63900 is 17:45 and so on. Integers must not be comma-separated.

### end

The end time is also an **integer** representing the time of day in seconds elapsed. The end of the day should be 86400. Each time period should be discrete, although adjacent end/start times can be the same. If the time periods of two actions overlap, the first applicable one will be used. Actions should be listed in chronological order.

### setpoint

The setpoint is an **integer** representing a value in milliWatts. A positive value represents a charging power target setpoint, while a negative value is a discharging power target, with a maximum of 3000000 and a minimum of -3000000, corresponding to charging 3kW of power and inverting -3kW respectively. The setpoint is an optional extra value that can be passed with force-charge and force-discharge. It is not required: the unit will charge and discharge according to default unit settings in the absence of a setpoint.

So a day's list of actions might be:

```json
"tuesday": [
  {"state": "force-charge", "start": 0, "end": 14400},
  {"state": "normal", "start": 14400, "end": 57600},
  {"state": "force-discharge", "start": 57600, "end": 68400, "setpoint": -3000000},
  {"state": "normal", "start": 68400, "end": 86400}
]
```

In the case above, the unit is being told to charge from grid supply between midnight and 4:00am, then go into normal operation from then until 4:00pm. Between 4:00pm and 7:00pm it discharges power with a target setpoint of 3.0 kW, then reverts to normal operation until midnight.

Below is an example of a complete Schedule message, which uses the schedule above on weekdays and remains in normal mode at weekends:

```json
{
  "schedule": {
    "monday": [
      {"state": "force-charge", "start": 0, "end": 14400},
      {"state": "normal", "start": 14400, "end": 57600},
      {"state": "force-discharge", "start": 57600, "end": 68400, "setpoint": -3000000},
      {"state": "normal", "start": 68400, "end": 86400}
    ],
    "tuesday": [
      {"state": "force-charge", "start": 0, "end": 14400},
      {"state": "normal", "start": 14400, "end": 57600},
      {"state": "force-discharge", "start": 57600, "end": 68400, "setpoint": -3000000},
      {"state": "normal", "start": 68400, "end": 86400}
    ],
    "wednesday": [
      {"state": "force-charge", "start": 0, "end": 14400},
      {"state": "normal", "start": 14400, "end": 57600},
      {"state": "force-discharge", "start": 57600, "end": 68400, "setpoint": -3000000},
      {"state": "normal", "start": 68400, "end": 86400}
    ],
    "thursday": [
      {"state": "force-charge", "start": 0, "end": 14400},
      {"state": "normal", "start": 14400, "end": 57600},
      {"state": "force-discharge", "start": 57600, "end": 68400, "setpoint": -3000000},
      {"state": "normal", "start": 68400, "end": 86400}
    ],
    "friday": [
      {"state": "force-charge", "start": 0, "end": 14400},
      {"state": "normal", "start": 14400, "end": 57600},
      {"state": "force-discharge", "start": 57600, "end": 68400, "setpoint": -3000000},
      {"state": "normal", "start": 68400, "end": 86400}
    ],
    "saturday": [
      {"state": "normal", "start": 0, "end": 86400}
    ],
    "sunday": [
      {"state": "normal", "start": 0, "end": 86400}
    ]
  }
}
```

## State Override messages

A `stateOverride` message can be sent to control a unit or group of units for a short duration, as a one-off action. Whereas a unit's schedule dictates its normal charging and discharging behaviour on an ongoing basis, a state override applies once, overriding the schedule state for the given duration. The JSON packet, if valid, is saved locally. When its duration expires, the override instruction is deleted, and the unit reverts to its schedule.

### requiredState

A **string** with the same value choices as "state" as defined above, representing the required charging/discharging state of the unit.

### timeout

An **integer** representing the duration of the override instruction in **milliseconds**. The override instruction will take effect immediately when the message is received by the unit.

### power

An **integer** like the setpoint for schedules, representing the target power setpoint in **milliWatts**.

A typical override message might be:

```json
{
  "stateOverride": {
    "requiredState": "force-discharge",
    "timeout": 300000,
    "power": -3000000
  }
}
```

In this case, the unit will invert for 5 minutes (300 seconds), with a target discharging power of 3.0 kW.

# Field Names / Test Plan Mapping

| Ref | Description | Field Name | Present? |
| - | - | - | - |
| KR 4.2.1 | Current 'Mode of Operation' | `Scheduler_RequestedState_INST` | Available |
| KR 4.2.2 | Available Charging Power [W] | `M4_None_ActiveChgPower_INST_W` | Available |
| KR 4.2.3 | Available Discharging Power [W] | `M4_None_ActiveDchgPower_INST_W` | Available |
| KR 4.3.1 | Battery SoC [%] measurement | `M4_undefined_StateOfCharge_INST_%` | Available |
| KR 4.3.3 | Battery DC Cumulative Energy Charged [Wh] | `DCBUS_None_ChgEnergy_INST_Wh` | Available |
| KR 4.3.4 | Battery DC Cumulative Energy Discharged [Wh] | `DCBUS_None_DchgEnergy_INST_Wh` | Available |
| KR 4.4.1 | Local Active Power [mW] | `FFR_LOCAL_ActivePower_INST_mW` | Available |
| KR 4.4.2 | Local Apparent Power [mVA] | `FFR_LOCAL_ApparentPower_INST_mW` | Available |
| KR 4.4.3 | Local RMS Current [mA] | `FFR_LOCAL_AcCurrent_INST_mA` | Available |
| KR 4.4.4 | Local RMS Voltage [mV] | `FFR_LOCAL_DcVoltage_INST_mV` | Available |
| KR 4.4.5 | Local AC Cumulative Charging Energy [Wh] | `FFR_LOCAL_None_ChgEnergy_INST_Wh` | Available |
| KR 4.4.6 | Local AC Cumulative Discharging Energy [Wh] | `FFR_LOCAL_None_DchgEnergy_INST_Wh` | Available |
| KR 4.5.1 | Grid Active Power [mW] | `FFR_HOUSE_ActivePower_INST_mW` | Available |
| KR 4.5.2 | Grid Apparent Power [mVA] | `FFR_HOUSE_ApparentPower_INST_mW` | Available |
| KR 4.5.3 | Grid RMS Current [mA] | `FFR_HOUSE_AcCurrent_INST_mA` | Available |
| KR 4.5.4 | Grid AC Frequency [mHz] | `FFR_LOCAL_AcFrequency_INST_mHz` | Available |
| KR 4.6.1 | Solar Active Power [mW] | `FFR_AUX1_ActivePower_INST_mW` | Available |
| KR 4.6.2 | Solar Apparent Power [mVA] | `FFR_AUX1_ApparentPower_INST_mW` | Available |
| KR 4.6.3 | Solar RMS Current [mA] | `FFR_AUX1_AcCurrent_INST_mA` | Available |

## Powervault Charing Modes

### normal

The Powervault will charge if there is surplus solar generation available and discharge to offset the energy demand from the grid

### force-charge

The Powervault will charge at max power until full (or at a given target power setpoint, if one is provided), whether or not surplus solar generation is available, and will not discharge. This is useful for overnight charging from the grid.

### force-discharge

The Powervault will discharge at max power until empty (or at a given target power setpoint if one is provided), with any energy surplus to consumption being exported to the grid, and will not charge.

### only-charge

The Powervault will charge from solar if the house is net exporting to the grid, but will not discharge to offset demand, therefore retaining energy in store. It will only charge from solar input; it will not draw power from mains supply.

### only-discharge

The Powervault will discharge energy to offset demand from the grid, but will not charge.

### ffr

The Powervault will discharge a variable amount of power at a target setpoint, based on the frequency of the grid. As grid frequency fluctuates, the Powervault's target power will update according to a predefined set of lookup values.

### disable

The Powervault will remain idle in a standby state.


