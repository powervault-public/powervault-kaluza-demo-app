const IotTestDevice = require("./classes/IotTestDevice");
const testSchedule = require("./test-messages/testSchedule.json");
const testStateOverride = require("./test-messages/testStateOverride.json");

const KaluzaBackend = new IotTestDevice({
  host: "a2ts3lusex0oae-ats.iot.eu-west-1.amazonaws.com",
  clientId: "KaluzaBackend",
  keyPath: "./certs/##########-private.pem.key", // <------------- Path to your private key
  certPath: "./certs/##########-certificate.pem.crt", // <-------- Path to your IoT certificate
  caPath: "./certs/AmazonRootCA1.pem",
  onConnect: () => {
    KaluzaBackend.subscribe("p3/kaluza/#");
    KaluzaBackend.subscribe("groups/kaluza/#");
    // sendTestSchedule(); // <----------------------------------- Uncomment to send test schedule
    // sendTestStateOverride(); // <------------------------------ Uncomment to send test state override
  },
  onMessage: (topic, message) => {
    // TODO: Handle data here!  Perhaps send to a Kinesis stream?
  }
});

function sendTestSchedule() {
  const topic = "p3/kaluza/005ba7e7/schedule"; // Specific unit
  // const topic = "groups/kaluza/a-custom-group/schedule" // Group of units
  KaluzaBackend.publish(topic, JSON.stringify(testSchedule));
}

function sendTestStateOverride() {
  const topic = "p3/kaluza/005ba7e7/stateOverride"; // Specific unit
  // const topic = "groups/kaluza/a-custom-group/stateOverride" // Group of units
  KaluzaBackend.publish(topic, JSON.stringify(testStateOverride));
}
