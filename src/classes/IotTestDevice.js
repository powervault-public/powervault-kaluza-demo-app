"use strict";

const awsIot = require("aws-iot-device-sdk");

class IotTestDevice {
  constructor(options) {
    this.config = {
      keyPath: options.keyPath,
      certPath: options.certPath,
      caPath: options.caPath,
      clientId: options.clientId,
      host: options.host,
      onConnect: options.onConnect,
      onMessage: options.onMessage
    };

    this.device = new awsIot.device({
      keyPath: this.config.keyPath,
      certPath: this.config.certPath,
      caPath: this.config.caPath,
      clientId: this.config.clientId,
      host: this.config.host
    });

    this.device.on("connect", () => {
      console.log(`${this.config.clientId}: Connected to ${this.config.host}`);
      if (typeof this.config.onConnect === "function") this.config.onConnect();
    });

    this.device.on("message", (topic, payload) => {
      console.log(`${this.config.clientId}: Message received`, topic, payload.toString());
      if (typeof this.config.onMessage === "function") this.config.onMessage(topic, payload);
    });
  }

  subscribe(topic) {
    this.device.subscribe(topic);
    console.log(`${this.config.clientId}: Subscribed to ${topic}`);
  }

  publish(topic, message) {
    const parsedMessage = typeof message === "string" ? message : JSON.stringify(message);
    this.device.publish(topic, parsedMessage);
    console.log(`${this.config.clientId}: Message sent to ${topic}`);
  }
}

module.exports = IotTestDevice;
