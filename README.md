# Powervault/Kaluza Demo App

A Node.js demo app for integrating with Powervault's MQTT service.

## Installation

`npm install`

## Certificates

The `certs` folder isn't included in the repo. You must create it and put your certificates in there, including Amazon's root CA. The certificates are referenced in `src/index.js`

You will need to set the `keyPath` and `certPath` parameters.

## Usage

`npm start`

or

`node src/index`
